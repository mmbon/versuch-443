# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 17:26:31 2020

@author: Roman
"""


import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from scipy.stats import chisquare


figure(figsize=(10,7), dpi=80)

files = glob.glob('*/*.csv')

print(files[4])

tau=np.loadtxt(files[4],skiprows=2,delimiter=',',usecols=0)
U=np.loadtxt(files[4],skiprows=2,delimiter=',', usecols=1)

dtau=[0.1]*len(tau)
dU=[0.3]*len(U)

print(len(tau))
tauk=tau[510:]
Uk=U[510:]

dtauk=[0.1]*len(tauk)
dUk=[0.3]*len(Uk)

def f(x,m,n):
    return m*(np.exp(-x/n))

x=[18,0.0038]

popt, pcov=curve_fit(f,tauk,Uk,p0=x,sigma=dUk,absolute_sigma=True)
perr=np.sqrt(np.diag(pcov))
plt.plot(tauk,f(tauk,*popt))
#plt.plot(tauk,f(tauk,18,0.0038))

print(chisquare(Uk,f_exp=f(tauk,*popt)))

print(popt.item(0))
print(popt.item(1))



plt.plot(tau,U)

#plt.errorbar(tau,U,xerr=0.03*tau,yerr=0.03*U, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')

#plt.errorbar(tum,umax,xerr=0.03*tum,yerr=0.03*umax, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')
plt.grid(which='minor',linewidth=0.2)
plt.grid(which='major')