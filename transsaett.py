# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 19:59:46 2020

@author: Roman
"""

import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from scipy.stats import chisquare


figure(figsize=(10,7), dpi=80)

files = glob.glob('*/*.csv')

umax=np.empty(26)
tum=np.array([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,25,30,35,40,45,50])

for i in range(5,31):
          
    tau=np.loadtxt(files[i],skiprows=2,delimiter=',',usecols=0)
    U=np.loadtxt(files[i],skiprows=2,delimiter=',', usecols=1)
    peaks, _=find_peaks(U,height=5)
    #print(peaks,U[peaks.item(1)])
    umax[i-5]=U[peaks.item(1)]
    print(peaks.item(1)-peaks.item(0),i)
    
#plt.errorbar(tau,U,xerr=0.03*tau,yerr=0.03*U, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')

dumax=[0.2]*len(umax)
dtum=[0.1]*len(umax)

def f(x,m,n):
    return m*(1-np.exp(-x/n))

popt, pcov=curve_fit(f,tum,umax,sigma=dumax,absolute_sigma=True)
perr=np.sqrt(np.diag(pcov))
plt.plot(tum,f(tum,*popt))


print(chisquare(umax,f_exp=f(tum,*popt)))


print(popt)
print(perr)


plt.errorbar(tum,umax,xerr=dtum,yerr=dumax, ls='', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')
#plt.plot(tum,umax)
plt.grid(which='minor',linewidth=0.2)
plt.grid(which='major')