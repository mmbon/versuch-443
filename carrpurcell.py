# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 17:26:31 2020

@author: Roman
"""


import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import find_peaks

figure(figsize=(10,7), dpi=80)

files = glob.glob('*/*.csv')

print(files[71])

tau=np.loadtxt(files[4],skiprows=2,delimiter=',',usecols=0)
U=np.loadtxt(files[4],skiprows=2,delimiter=',', usecols=1)

def f(x,m,n):
    return m*(1-np.exp(-x/n))

popt, pcov=curve_fit(f,tum,umax,sigma=0.03*umax,absolute_sigma=True)
perr=np.sqrt(np.diag(pcov))
#plt.plot(tum,f(tum,*popt))


print(popt.item(0))
print(popt.item(1))

plt.plot(tau,U)

#plt.errorbar(tau,U,xerr=0.03*tau,yerr=0.03*U, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')

#plt.errorbar(tum,umax,xerr=0.03*tum,yerr=0.03*umax, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')
plt.grid(which='minor',linewidth=0.2)
plt.grid(which='major')