# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 15:56:32 2020

@author: Roman
"""


import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from scipy.stats import chisquare


figure(figsize=(10,7), dpi=80)

files = glob.glob('*/*.csv')

umax=np.empty(10)
tum=np.array([2,4,6,8,10,12,14,16,18,20])


dumax=[0.3]*len(umax)
dtum=[0.1]*len(umax)

for i in range(58,68):
          
    tau=np.loadtxt(files[i],skiprows=2,delimiter=',',usecols=0)
    U=np.loadtxt(files[i],skiprows=2,delimiter=',', usecols=1)
    peaks, _=find_peaks(U,height=5)
    print(peaks,U[peaks.item(len(peaks)-1)])
    umax[i-58]=U[peaks.item(len(peaks)-1)]
    #print(peaks.item(1)-peaks.item(0),i)
    
#plt.errorbar(tau,U,xerr=0.03*tau,yerr=0.03*U, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')

def f(x,m,n):
    return m*np.exp(-2*x/n)

popt, pcov=curve_fit(f,tum,umax,sigma=dumax,absolute_sigma=True)
perr=np.sqrt(np.diag(pcov))
plt.plot(tum,f(tum,*popt))



plt.errorbar(tum,umax,xerr=dtum,yerr=dumax, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')
#plt.plot(tum,umax)
print(popt)
print(perr)

plt.grid(which='minor',linewidth=0.2)
plt.grid(which='major')



print(chisquare(umax,f_exp=f(tum,*popt)))


figure(figsize=(10,7), dpi=80)

j=67
print(files[j])

tautest=np.loadtxt(files[j],skiprows=2,delimiter=',',usecols=0)
Utest=np.loadtxt(files[j],skiprows=2,delimiter=',', usecols=1)

plt.errorbar(tautest,Utest,xerr=0.03*tautest,yerr=0.03*Utest, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')

plt.grid(which='minor',linewidth=0.2)
plt.grid(which='major')