# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 18:18:52 2020

@author: Roman
"""



import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from scipy.optimize import curve_fit
from scipy.stats import chisquare



figure(figsize=(10,7), dpi=80)

Alen = np.loadtxt('rabi.csv', skiprows=1,delimiter=',', usecols=0)
uenv = np.loadtxt('rabi.csv', skiprows=1,delimiter=',', usecols=1)
duenv = np.loadtxt('rabi.csv', skiprows=1,delimiter=',', usecols=2)
ui = np.loadtxt('rabi.csv', skiprows=1,delimiter=',', usecols=3)
dui = np.loadtxt('rabi.csv', skiprows=1,delimiter=',', usecols=4)

x=Alen
dx=[0.01]*len(Alen)

y1=uenv-0.233
dy1=duenv

y2=ui-0.233
dy2=dui

def f(x,m,n):
    return m*np.sin(n*x)

def g(x,m,n,a):
    return a+m*np.abs(np.sin(n*x))

xplot=np.arange(0.1,12,0.1)

popt, pcov=curve_fit(f,x,y2,p0=[1.3,-0.8],sigma=dy2,absolute_sigma=True)
perr=np.sqrt(np.diag(pcov))
plt.plot(x,f(x,*popt))

print(*popt,perr)
print(chisquare(xplot,f_exp=f(xplot,*popt)))


popt, pcov=curve_fit(g,x,y1,p0=[1.3,0.87,3],sigma=dy1,absolute_sigma=True)
perr=np.sqrt(np.diag(pcov))
plt.plot(xplot,g(xplot,*popt))

print(*popt,perr)
print(chisquare(x,f_exp=g(x,*popt)))


plt.errorbar(x,y1,xerr=dx,yerr=dy1, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')
plt.errorbar(x,y2,xerr=dx,yerr=dy2, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='red',label='Input')

plt.grid(which='minor')
plt.grid(which='major')
#plt.loglog()
plt.xlabel('Länge')
plt.ylabel('Spannung')
plt.legend()