# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 15:12:13 2020

@author: Roman
"""


import glob
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from scipy.stats import chisquare


figure(figsize=(10,7), dpi=80)

files = glob.glob('*/*.csv')

umax=np.empty(26)
tum=np.array([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,25,30,35,40,45,50])


dumax=[0.3]*len(umax)
dtum=[0.1]*len(umax)

for i in range(31,57):
        
    tau=np.loadtxt(files[i],skiprows=2,delimiter=',',usecols=0)
    U=np.loadtxt(files[i],skiprows=2,delimiter=',', usecols=1)
    peaks, _=find_peaks(U,height=2)
    if i==53:
        umax[56-i]=-0.3

        continue
    if i==51:
        umax[56-i]=U[peaks.item(2)]
        continue
    if i==52:
        umax[56-i]=U[peaks.item(4)]
        continue
    if i==54:
        umax[56-i]=-U[peaks.item(5)]
        continue
    
    if i==55:
        umax[56-i]=-U[peaks.item(1)]
        continue
    if i==56:
        umax[56-i]=-U[peaks.item(3)]    
        print(peaks.item(3)-peaks.item(0),i)

        continue
    
 #   print(peaks,U[peaks.item(1)],i)
    print(peaks.item(1)-peaks.item(0),i)

    umax[56-i]=U[peaks.item(1)]
    
#plt.errorbar(tau,U,xerr=0.03*tau,yerr=0.03*U, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')



def f(x,m,n):
    return m*(1-2*np.exp(-x/n))

popt, pcov=curve_fit(f,tum,umax,sigma=dumax,absolute_sigma=True)
perr=np.sqrt(np.diag(pcov))
plt.plot(tum,f(tum,*popt))


print(chisquare(umax,f_exp=f(tum,*popt)))


print(popt)
print(perr)


plt.errorbar(tum,umax,xerr=dtum,yerr=dumax, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')

plt.grid(which='minor',linewidth=0.2)
plt.grid(which='major')

print(files[56])


figure(figsize=(10,7), dpi=80)

j=56

tautest=np.loadtxt(files[j],skiprows=2,delimiter=',',usecols=0)
Utest=np.loadtxt(files[j],skiprows=2,delimiter=',', usecols=1)

plt.errorbar(tautest,Utest,xerr=0.03*tau,yerr=0.03*U, ls='none', capsize=2,elinewidth=0.5, capthick=0.5, color='blue',label='Envelope')

plt.grid(which='minor',linewidth=0.2)
plt.grid(which='major')